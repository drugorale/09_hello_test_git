package test.java.gf.my_test;

import static org.junit.Assert.*;  
import main.java.gf.my_test.Book;
import main.java.gf.my_test.BookInterface;

import org.junit.After;  
import org.junit.Before;  
import org.junit.Ignore;
import org.junit.Test;  

public class BookTest {
	
	Book book;
	public final int BOOK_PAGES_NUMBER = 2;
	
	@Before  
    public void beforeTest() {  
        book=new Book(BOOK_PAGES_NUMBER);  
    }  
  
    @After  
    public void afterTest() {  
        book = null;  
    }  
  
    @Test(expected = IllegalArgumentException.class)  
    public void testAbstractContainer() {  
    	BookInterface bookError = new Book(0);  
    }  
    
    @Test
    public void bookTest() {  
    	assertTrue("Test scroll (to page number 2)", book.scrollForward());
    	assertEquals("Book at page 2", book.getCurrentPage(), 2);
    	assertFalse("Test scroll (no more page)", book.scrollForward());
    }
    
    //ignore
    @Ignore("Not Ready to Run")  
    @Test
    public void divisionWithException() {  
      System.out.println("Method is not ready yet");
    }  

    //time limit
    
//    @Test(timeout = 1000)  
//    public void infinity() {  
//        while (true){
//        	
//        }
//    }  
	
}
