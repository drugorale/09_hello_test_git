package test.java.gf.my_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)  
@SuiteClasses({ BookTest.class, BookMockTest.class })  
public class AllTests {  
  
}  